#include <cuda.h>
#include "cuda_runtime.h"
#include <stdio.h>

#define BLOCK_SIZE      16
#define FILTER_WIDTH    3       
#define FILTER_HEIGHT   3       

using namespace std;

__global__ void sobelFilter(unsigned char *srcImage, unsigned char *dstImage, unsigned int width, unsigned int height)
{
   int x = blockIdx.x*blockDim.x + threadIdx.x;
   int y = blockIdx.y*blockDim.y + threadIdx.y;

   float Kx[3][3] = {1, 0, -1, 2, 0, -2, 1, 0, -1};
   float Ky[3][3] = {1, 2, 1, 0, 0, 0, -1, -2, -1};

   // only threads inside image will write results
   if((x>=FILTER_WIDTH/2) && (x<(width-FILTER_WIDTH/2)) && (y>=FILTER_HEIGHT/2) && (y<(height-FILTER_HEIGHT/2)))
   {
         // Gradient in x-direction 
         float Gx = 0;
         // Loop inside the filter to average pixel values
         for(int ky=-FILTER_HEIGHT/2; ky<=FILTER_HEIGHT/2; ky++) {
            for(int kx=-FILTER_WIDTH/2; kx<=FILTER_WIDTH/2; kx++) {
               float fl = srcImage[((y+ky)*width + (x+kx))];
               Gx += fl*Kx[ky+FILTER_HEIGHT/2][kx+FILTER_WIDTH/2];
            }
         }
         float Gx_abs = Gx < 0 ? -Gx : Gx;

         // Gradient in y-direction 
         float Gy = 0;
         // Loop inside the filter to average pixel values
         for(int ky=-FILTER_HEIGHT/2; ky<=FILTER_HEIGHT/2; ky++) {
            for(int kx=-FILTER_WIDTH/2; kx<=FILTER_WIDTH/2; kx++) {
               float fl = srcImage[((y+ky)*width + (x+kx))];
               Gy += fl*Ky[ky+FILTER_HEIGHT/2][kx+FILTER_WIDTH/2];
            }
         }
         float Gy_abs = Gy < 0 ? -Gy : Gy;

         dstImage[(y*width+x)] =  Gx_abs + Gy_abs;
   }
}

//Helper function for using CUDA to compute Sobel operator 
extern "C" void sobelFilter_GPU(unsigned char* input, unsigned char* output, int cols, int rows)
{
        // Calculate number of input & output bytes in each block
        const int inputSize = cols * rows;
        const int outputSize =cols * rows;
        unsigned char *d_input, *d_output;
        cudaError_t cudaStatus;
	 // Specify block size
        const dim3 block(BLOCK_SIZE,BLOCK_SIZE);

        // Calculate grid size to cover the whole image
        const dim3 grid((cols + block.x - 1)/block.x, (rows + block.y - 1)/block.y);	
	
	//Choose which GPU to run on, change this on a multi-GPU system.
        cudaStatus = cudaSetDevice(0);
        if (cudaStatus != cudaSuccess) {
	        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
	        goto Error;
	    }
         cudaDeviceProp prop;      // CUDA device properties variable
	 cudaGetDeviceProperties( &prop, 0); 

        // Allocate device memory
        cudaStatus = cudaMalloc<unsigned char>(&d_input,inputSize);
	if (cudaStatus != cudaSuccess) {
	         fprintf(stderr, "cudaMalloc failed!");
	         goto Error;
	     }

        cudaStatus = cudaMalloc<unsigned char>(&d_output,outputSize);
	if (cudaStatus != cudaSuccess) {
		 fprintf(stderr, "cudaMalloc failed!");
	         goto Error;
	     }


        // Copy data from OpenCV input image to device memory
        cudaStatus = cudaMemcpy(d_input,input,inputSize,cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
       	      }
        // Specify block size
        //const dim3 block(BLOCK_SIZE,BLOCK_SIZE);

        // Calculate grid size to cover the whole image
        //const dim3 grid((cols + block.x - 1)/block.x, (rows + block.y - 1)/block.y);

        // Run Sobel Edge Detection Filter kernel on CUDA 
        sobelFilter<<<grid,block>>>(d_input, d_output, cols, rows);

	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "sobelFilter kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		goto Error;
		}
// cudaDeviceSynchronize waits for the kernel to finish, and returns
// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching Kernel!\n", cudaStatus);
		goto Error;
		}

        //Copy data from device memory to output image
        cudaStatus = cudaMemcpy(output,d_output,outputSize,cudaMemcpyDeviceToHost);
        if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
		}

        //Free the device memory
Error:
        cudaFree(d_input);
        cudaFree(d_output);

//	return cudaStatus;

}













