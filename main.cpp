#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/types_c.h"
#include <opencv2/highgui.hpp>
#include <iostream>
#include <string>
#include <stdio.h>
#include <chrono>

using namespace std;
using namespace cv;

int MAX_KERNEL_LENGTH = 9;

extern "C" bool sobelFilter_GPU(unsigned char* input, unsigned char* output, int cols, int rows);
void sobelFilter_CPU(const cv::Mat& input, cv::Mat& output)
{
	Point anchor = Point( -1, -1 );
	double delta = 0;
	int ddepth = -1;
	int kernel_size;

// Update kernel size for a normalized box filter
   kernel_size = 3; 
      
   cv::Mat output1;
   cv::Mat kernel1 = (Mat_<double>(kernel_size,kernel_size) << 1, 0, -1, 2, 0, -2, 1, 0, -1);
// Apply 2D filter on x direction
   filter2D(input, output1, ddepth, kernel1, anchor, delta, BORDER_DEFAULT );
                
   cv::Mat output2;
   cv::Mat kernel2 = (Mat_<double>(kernel_size,kernel_size) << 1, 2, 1, 0, 0, 0, -1, -2, -1);
// Apply 2D filter on y direction
   filter2D(input, output2, ddepth, kernel2, anchor, delta, BORDER_DEFAULT );

   output = output1 + output2;

   }

// Program main
int main( int argc, char** argv ) {

   // name of image
   string image_name = "lighthouse";

   // input & output file names
   string input_file =  image_name+".jpg";
   string output_file_cpu = image_name+"_cpu.jpeg";
   string output_file_gpu = image_name+"_gpu.jpeg";

   // Read input image 
   cv::Mat srcImage = cv::imread(input_file );
   if(srcImage.empty())
   {
      std::cout<<"Image Not Found: "<< input_file << std::endl;
      return -1;
   }
   cout <<"\ninput image size: "<<srcImage.cols<<" "<<srcImage.rows<<" "<<srcImage.channels()<<"\n";

   // convert RGB to gray scale
   cv::cvtColor(srcImage, srcImage, CV_BGR2GRAY);
  
   // Declare the output image  
   cv::Mat dstImage (srcImage.size(), srcImage.type());

   // run sobel edge detection filter on GPU
   std::chrono::time_point<std::chrono::system_clock> startGPU;
   std::chrono::time_point<std::chrono::system_clock> finishGPU;
   std::chrono::duration<double> elapsed_secondsGPU;
   startGPU = std::chrono::system_clock::now();

   sobelFilter_GPU(srcImage.ptr(), dstImage.ptr(),srcImage.cols,srcImage.rows);
   finishGPU = std::chrono::system_clock::now();
   elapsed_secondsGPU = finishGPU - startGPU;
   std::cout << "Time GPU : " << elapsed_secondsGPU.count() << " seconds." << "\n\n";

   // normalization to 0-255
   dstImage.convertTo(dstImage, CV_32F, 1.0 / 255, 0);
   dstImage*=255;
  
   // Output image
   imwrite(output_file_gpu, dstImage);
   startGPU = std::chrono::system_clock::now();
  
   // run sobel edge detection filter on CPU  
   sobelFilter_CPU(srcImage, dstImage);
   finishGPU = std::chrono::system_clock::now();
   elapsed_secondsGPU = finishGPU - startGPU;
   std::cout << "Time CPU : " << elapsed_secondsGPU.count() << " seconds." << "\n";
  
   // normalization to 0-255
   dstImage.convertTo(dstImage, CV_32F, 1.0 / 255, 0);
   dstImage*=255;
  
   // Output image
   imwrite(output_file_cpu, dstImage);
     
   return 0;
}





