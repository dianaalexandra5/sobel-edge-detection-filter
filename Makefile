CFLAGS = `pkg-config --cflags opencv4`
LIBS = `pkg-config --libs opencv4`
all:
	nvcc -c sobelEdgeDetectionFilter.cu 
	g++ --std=c++11 -o sobel main.cpp sobelEdgeDetectionFilter.o  `pkg-config opencv4 --cflags --libs` -lcudart -lcuda

clean:
	rm -rf *.o sobel *_cpu.jpeg *_gpu.jpeg
